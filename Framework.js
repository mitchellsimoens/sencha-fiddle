'use strict';

const { Batch } = require('@extjs/sencha-mysql');

const Base              = require('./Base');
const FrameworkCombiner = require('./combiner/Framework');
const Manager           = require('./Manager');

/**
 * @class Sencha.fiddle.Framework
 * @extends Sencha.fiddle.Base
 *
 * A class that can load the framework resources.
 */
class Framework extends Base {
    static get meta () {
        return {
            prototype : {
                /**
                 * @readonly
                 * @property {Boolean} [isFramework=true]
                 */
                isFramework : true
            }
        };
    }

    /**
     * @static
     * @method
     * @param {Object} info
     * @param {Number} info.id The framework id to load the associated framework.
     * @param {Sencha.mysql.Batch} info.batch Optional. The batch to use to
     * add the separate database queries to. If one is not provided, one will
     * automatically be instantiated.
     * @param {Sencha.mysql.Connection} info.connection Optional. The database
     * connection to use to retrieve the data. If not provided, will not execute
     * the queries.
     * @return {Promise}
     */
    static load (info) {
        return new Promise((resolve, reject) => {
            //TODO support the object from the docs

            if (info.id) {
                let id                    = info.id,
                    connection            = info.connection,
                    batch                 = info.batch || new Batch(),
                    frameworkCombiner     = new FrameworkCombiner({
                        nightly : info.nightly
                    }),
                    framework             = Manager.instantiateOperation('framework.get'),
                    frameworkAsset        = Manager.instantiateOperation('framework.asset.get'),
                    frameworkPackage      = Manager.instantiateOperation('framework.package.get'),
                    frameworkPackageAsset = Manager.instantiateOperation('framework.package.asset.get');

                frameworkCombiner.add('framework',                  framework            .getById            (id, batch));
                frameworkCombiner.add('framework.assets',           frameworkAsset       .getById            (id, batch));
                frameworkCombiner.add('framework.packages',         frameworkPackage     .getForFrameworkById(id, batch));
                frameworkCombiner.add('framework.packages.$assets', frameworkPackageAsset.getForPackageById  (id, batch));

                frameworkCombiner
                    .then(data => new this({
                        data : data
                    }))
                    .then(resolve, reject);

                if (connection) {
                    connection.exec(batch);
                }
            } else {
                reject(new Error('No `id` to load.'));
            }
        });
    }

    /**
     * @static
     * @method
     * @param {Object} info
     * @param {Number} info.id The real fiddle id to load the associated framework.
     * @param {Sencha.mysql.Batch} info.batch Optional. The batch to use to
     * add the separate database queries to. If one is not provided, one will
     * automatically be instantiated.
     * @param {Sencha.mysql.Connection} info.connection Optional. The database
     * connection to use to retrieve the data. If not provided, will not execute
     * the queries.
     * @return {Promise}
     */
    static loadForFiddle (info) {
        return new Promise((resolve, reject) => {
            if (info.id) {
                let id                    = info.id,
                    connection            = info.connection,
                    batch                 = info.batch || new Batch(),
                    frameworkCombiner     = new FrameworkCombiner({
                        nightly : info.nightly
                    }),
                    framework             = Manager.instantiateOperation('framework.get'),
                    frameworkAsset        = Manager.instantiateOperation('framework.asset.get'),
                    frameworkPackage      = Manager.instantiateOperation('framework.package.get'),
                    frameworkPackageAsset = Manager.instantiateOperation('framework.package.asset.get');

                frameworkCombiner.add('framework',                  framework            .getForFiddle            (id, batch));
                frameworkCombiner.add('framework.assets',           frameworkAsset       .getForFiddle            (id, batch));
                frameworkCombiner.add('framework.packages',         frameworkPackage     .getForFrameworkForFiddle(id, batch));
                frameworkCombiner.add('framework.packages.$assets', frameworkPackageAsset.getForPackageForFiddle  (id, batch));

                frameworkCombiner
                    .then(data => new this({
                        data : data
                    }))
                    .then(resolve, reject);

                if (connection) {
                    connection.exec(batch);
                }
            } else {
                reject(new Error('No `id` to load.'));
            }
        });
    }
}

module.exports = Framework;
