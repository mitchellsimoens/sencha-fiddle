'use strict';

module.exports = {
    get Adapter () {
        return require('./Adapter');
    },

    get Base () {
        return require('./Base');
    },

    get Fiddle () {
        return require('./Fiddle');
    },

    get File () {
        return require('./File')
    },

    get Framework () {
        return require('./Framework');
    },

    get Manager () {
        return require('./Manager');
    },

    get Package () {
        return require('./Package');
    },

    get Permission () {
        return require('./Permission');
    },

    get combiner () {
        return {
            get Combiner () {
                return require('./combiner/Combiner');
            },

            get Fiddle () {
                return require('./combiner/Fiddle');
            },

            get Framework () {
                return require('./combiner/Framework');
            },

            get Package () {
                return require('./combiner/Package');
            }
        }
    }
};
