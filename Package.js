'use strict';

const { Batch } = require('@extjs/sencha-mysql');

const Base            = require('./Base');
const Manager         = require('./Manager');
const PackageCombiner = require('./combiner/Package');

class Package extends Base {
    static get meta () {
        return {
            prototype : {
                /**
                 * @readonly
                 * @property {Boolean} [isPackage=true]
                 */
                isPackage : true
            }
        };
    }

    static loadByNames (info) {
        return new Promise((resolve, reject) => {
            const names = info.names;

            if (Array.isArray(names) && names.length) {
                let framework  = info.framework,
                    connection = info.connection,
                    batch      = info.batch || new Batch(),
                    pkg        = Manager.instantiateOperation('framework.package.get'),
                    assets     = Manager.instantiateOperation('framework.package.asset.get');

                if (typeof framework === 'number') {
                    framework = {
                        id : framework
                    };
                }

                Promise
                    .all(
                        names.map(name => {
                            let packageCombiner = new PackageCombiner({
                                    nightly : info.nightly
                                }),
                                descriptor      = Object.assign(
                                    {
                                        name : name
                                    },
                                    framework
                                );

                            packageCombiner.add('package',         pkg   .getForFrameworkById(descriptor, batch));
                            packageCombiner.add('package.$assets', assets.getForPackageById  (descriptor, batch));

                            return packageCombiner;
                        })
                    )
                    /**
                     * since we use Promise.all, packages comes back as:
                     *
                     *     [
                     *         [
                     *             {...}
                     *         ],
                     *         [
                     *             {...}
                     *         ]
                     *     ]
                     *
                     * so we need to flatten it a bit
                     */
                    .then(packages => packages.map(pkg => pkg[0]))
                    .then(resolve, reject);

                if (connection) {
                    connection.exec(batch);
                }
            } else {
                resolve();
            }
        });
    }
}

module.exports = Package;
