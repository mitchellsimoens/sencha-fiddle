'use strict';

const Combiner          = require('./Combiner');
const FrameworkCombiner = require('./Framework');
const PackageCombiner   = require('./Package');

/**
 * @class Sencha.fiddle.combiner.Fiddle
 * @extends Sencha.fiddle.combiner.Combiner
 *
 * Handles combining a fiddle's resources.
 */
class Fiddle extends Combiner {
    static get meta () {
        return {
            prototype : {
                /**
                 * @readonly
                 * @property {Boolean} [isFiddleCombiner=true]
                 */
                isFiddleCombiner : true
            }
        };
    }

    /**
     * Checks if the fiddle has loaded all it's resources. Will
     * automatically destroy this combiner when all resources
     * have been loaded.
     */
    check () {
        let me = this;

        if (!me.count) {
            let deferred = me.deferred;

            if (me.hasError) {
                deferred.reject(me.hasError);
            } else {
                let fiddle            = me.data.fiddle,
                    framework         = fiddle.framework,
                    frameworkCombiner = new FrameworkCombiner({
                        nightly : fiddle.frameworkVersion
                    }),
                    packageCombiner   = new PackageCombiner();

                fiddle.framework = frameworkCombiner.preCombine(framework.data);
                fiddle.packages  = packageCombiner  .preCombine(fiddle.packages);

                deferred.resolve(fiddle);
            }

            me.destroy();
        }
    }
}

module.exports = Fiddle;
