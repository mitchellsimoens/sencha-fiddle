const dummyjson = require('dummy-json');

/** filter start */
const operatorFns    = {
    '<'       (value, filterValue) {
        return value < filterValue;
    },
    '<='      (value, filterValue) {
        return value <= filterValue;
    },
    '='       (value, filterValue) {
        return value == filterValue;
    },
    '==='     (value, filterValue) {
        return value === filterValue;
    },
    '>='      (value, filterValue) {
        return value >= filterValue;
    },
    '>'       (value, filterValue) {
        return value > filterValue;
    },
    '!='      (value, filterValue) {
        return value != filterValue;
    },
    '!=='     (value, filterValue) {
        return value !== filterValue;
    },
    'in'      (value, filterValue) {
        if (Array.isArray(filterValue)) {
            return filterValue.includes(value);
        }
    },
    'notin'   (value, filterValue) {
        if (Array.isArray(filterValue)) {
            return !filterValue.includes(value);
        }
    },
    'like'    (value, filterValue) {
        let regex;

        try {
            regex = new RegExp(filterValue, 'i');
        } catch (e) {}

        return regex ? regex.test(value) : false;
    },
    'notlike' (value, filterValue) {
        let regex;

        try {
            regex = new RegExp(filterValue, 'i');
        } catch (e) {}

        return regex ? !regex.test(value) : true;
    }
};
operatorFns['=='] = operatorFns['='];
operatorFns['/='] = operatorFns['like'];
operatorFns.gt    = operatorFns['>'];
operatorFns.ge    = operatorFns['>='];
operatorFns.lt    = operatorFns['<'];
operatorFns.le    = operatorFns['<='];
operatorFns.eq    = operatorFns['='];
operatorFns.ne    = operatorFns['!='];
const createFilterFn = (...args) => {
    return item => {
        return !args.some(filter => {
            const operator = filter.operator || '=',
                  fn       = operatorFns[operator.toLowerCase()];

            return fn(item[filter.property], filter.value, item, filter) === false;
        });
    }
};
/** filter end */

/** sort start */
const descRe        = /^desc$/i;
const defaultSortFn = (a, b) => {
    if (a === b) {
        return 0;
    }

    return a < b ? -1 : 1;
};
const getCmpFunc    = reverse => {
    if (reverse) {
        return (a, b) => {
            return -1 * defaultSortFn(a, b);
        };
    }

    return defaultSortFn;
};
const createSortFn  = (...args) => {
    const fields = args.map(field => {
        if (typeof field === 'string') {
            return {
                property : field,
                fn       : defaultSortFn
            };
        } else {
            return {
                property : field.property,
                fn       : getCmpFunc(descRe.test(field.direction))
            };
        }
    });

    return (A, B) => {
        let field, property, result;

        for (let i = 0, length = fields.length; i < length; i++) {
            field    = fields[i];
            property = field.property;
            result   = field.fn(A[property], B[property]);

            if (result !== 0) break;
        }

        return result;
    };
};
/** sort end */

/** sort & filter shared start */
const data = (data, sortInfo, filterInfo) => {
    if (filterInfo) {
        try {
            if (typeof filterInfo === 'string') {
                filterInfo = JSON.parse(filterInfo);
            }

            if (!Array.isArray(filterInfo)) {
                filterInfo = [filterInfo];
            }

            data = data.filter(
                createFilterFn.apply(this, filterInfo)
            );
        } catch (e) {}
    }

    if (sortInfo) {
        try {
            if (typeof sortInfo === 'string') {
                sortInfo = JSON.parse(sortInfo);
            }

            if (!filterInfo) {
                //filter creates a new array, sort does not
                data = data.slice();
            }

            if (!Array.isArray(sortInfo)) {
                sortInfo = [sortInfo];
            }

            data.sort(
                createSortFn.apply(this, sortInfo)
            );
        } catch (e) {}
    }

    return data;
};
const sort   = (arr, sort, filter) => data(arr, sort, filter);
const filter = (arr, filter, sort) => data(arr, sort, filter);

/** template start */
const arrayExtractRe  = /^[^[]*(\[[\s\S]*\])[^\]]*$/m;
const objectExtractRe = /^[^{]*({[\s\S]*})[^}]*$/m;
const stripComments   = code => {
    let objPos = code.indexOf('{'),
        arrPos = code.indexOf('['),
        re     = arrayExtractRe,
        re2    = objectExtractRe,
        data;

    if (objPos >= 0 && (arrPos < 0 || objPos < arrPos)) {
        // found-object && (no-array or object-before-array)
        re  = objectExtractRe;
        re2 = arrayExtractRe;
    }

    if (!(data = re.exec(code))) {
        data = re2.exec(code);
    }

    if (data) {
        try {
            code = JSON.parse(data[1]);
        } catch (e) {}
    }

    return code;
};
const defaultOptions = {
    stripComments : true
};
const template = (code, options) => {
    try {
        if (typeof code === 'string') {
            options = Object.assign({}, defaultOptions, options);

            let temp   = code,
                params = options.params;

            if (options.stripComments) {
                temp = stripComments(temp);
            }

            temp = dummyjson.parse(temp);

            if (temp && params) {
                let sort   = params.sort,
                    filter = params.filter;

                if (sort || filter) {
                    if (typeof temp === 'string') {
                        temp = JSON.parse(temp);
                    }

                    if (Array.isArray(temp)) {
                        temp = data(temp, sort, filter);
                    } else {
                        for (let key in temp) {
                            let value = temp[key];

                            if (Array.isArray(value)) {
                                temp[key] = data(value, sort, filter);
                            }
                        }
                    }

                    temp = JSON.stringify(temp);
                }
            }

            code = temp;
        }
    } catch (e) {}

    return code;
};
/** template end */

module.exports = {
    data,
    filter,
    sort,
    template
};
