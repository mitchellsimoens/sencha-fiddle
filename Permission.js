'use strict';

const { Batch } = require('@extjs/sencha-mysql');

const Base    = require('./Base');
const Manager = require('./Manager');

class Permission extends Base {
    static get meta () {
        return {
            prototype : {
                /**
                 * @readonly
                 * @property {Boolean} [isPermission=true]
                 */
                isPermission : true
            }
        };
    }

    static getForTeam (info) {
        return new Promise((resolve, reject) => {
            if (info.id && info.userid) {
                let connection = info.connection,
                    batch      = info.batch || new Batch(),
                    permission = Manager.instantiateOperation('permission.team.get');

                permission
                    .get(info.id, info.userid, batch)
                    .then(resolve, reject);

                if (connection) {
                    connection.exec(batch);
                }
            } else {
                reject(new Error('id and userid are required parameters'));
            }
        });
    }

    static getForFiddle (info) {
        return new Promise((resolve, reject) => {
            if (info.id && info.userid) {
                let connection = info.connection,
                    batch      = info.batch || new Batch(),
                    permission = Manager.instantiateOperation('permission.fiddle.get');

                permission
                    .get(info.id, info.userid, batch)
                    .then(resolve, reject);

                if (connection) {
                    connection.exec(batch);
                }
            } else {
                reject(new Error('id and userid are required parameters'));
            }
        });
    }
}

module.exports = Permission;
